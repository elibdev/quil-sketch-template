(ns sketch.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [sketch.dynamic :as dynamic]))

(q/defsketch example                
  :title dynamic/title
  :setup dynamic/setup
  :draw dynamic/draw
  :update dynamic/evolve
  :middleware [m/fun-mode]
  :size dynamic/size)
