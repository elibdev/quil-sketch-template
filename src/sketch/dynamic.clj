(ns sketch.dynamic
  (:require [quil.core :as q]))

(def title "CoOol Sketch")

(def size [<width> <height>])

(defn setup []
  ;; return the initial state for the sketch
  <state>)

(defn evolve [state]
  ;; return an updated state for the sketch
  <updated state>)

(defn draw [state]
  ;; draw something, probably using state
  <draw something>)
